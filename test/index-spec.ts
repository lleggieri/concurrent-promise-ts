import {all, execute} from '../lib';

function delay(ms: number): Promise<void> {
  return new Promise<void>((resolve) => {
    setTimeout(() => resolve(), ms);
  });
}

const tenNumbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

describe('all', () => {

  it('with no delay', async () => {
    const actualValues = await all([1, 2, 3, 4, 5, 6], ((n) => Promise.resolve(n)), 3);
    expect(actualValues).toEqual([1, 2, 3, 4, 5, 6]);
  });

  it('with 50ms delay', async () => {
    const actualValues = await all([1, 2, 3, 4, 5, 6], ((n) => delay(50).then(() => n)), 3);
    expect(actualValues).toEqual([1, 2, 3, 4, 5, 6]);
  });

  it('with default concurrency', async () => {
    const actualValues = await all([1, 2, 3, 4, 5, 6], ((n) => delay(50).then(() => n)));
    expect(actualValues).toEqual([1, 2, 3, 4, 5, 6]);
  });

  it('does not swallow errors', async () => {
    const error = new Error('boom 1');
    const f = async (n: number) => {
      if (n === 5) {
        throw error;
      }
      await delay(50);
      return n;
    };
    const promise = all([1, 2, 3, 4, 5, 6], f, 3);
    await expect(promise).rejects.toBe(error);
  });

  it('does not swallow errors at full concurrency', async () => {
    const error = new Error('boom 2');
    const f = async (n: number) => {
      await delay(50);
      if (n === 5) {
        throw error;
      }
      return n;
    };
    const promise = all([1, 2, 3, 4, 5, 6], f);
    await expect(promise).rejects.toBe(error);
  });

  it('fails with negative concurrency argument', async () => {
    const promise = all([1, 2, 3, 4, 5, 6], (n) => Promise.resolve(n), -1);
    await expect(promise).rejects.toThrow('Invalid concurrency value');
  });

  it('fails with invalid concurrency argument', async () => {
    const promise = all([1, 2, 3, 4, 5, 6], (n) => Promise.resolve(n), NaN);
    await expect(promise).rejects.toThrow('Invalid concurrency value');
  });

  it('with empty sources', async () => {
    const actualValues = await all([], ((n) => delay(50).then(() => n)));
    expect(actualValues).toEqual([]);
  });

});

describe('execute', () => {

  it('with no delay', async () => {
    const actualValues = new Array<number>();
    for await (const value of execute([1, 2, 3, 4, 5, 6], ((n) => Promise.resolve(n)), 3, false)) {
      actualValues.push(value);
    }
    expect(actualValues).toEqual([1, 2, 3, 4, 5, 6]);
  });

  it('with 50ms delay', async () => {
    const actualValues = new Array<number>();
    for await (const value of execute([1, 2, 3, 4, 5, 6], ((n) => delay(50).then(() => n)), 3, false)) {
      actualValues.push(value);
    }
    expect(actualValues).toEqual([1, 2, 3, 4, 5, 6]);
  });

  it('with default concurrency', async () => {
    const actualValues = new Array<number>();
    for await (const value of execute([1, 2, 3, 4, 5, 6], ((n) => delay(50).then(() => n)))) {
      actualValues.push(value);
    }
    expect(actualValues).toEqual([1, 2, 3, 4, 5, 6]);
  });

  it('respects concurrency maximum value', async () => {
    const actualValues = new Array<number>();
    const concurrency = 17;
    let inFlight = 0;
    const values = [...Array(100).keys()];
    let exceededLimit = false;
    const f = async (n: number) => {
      if (inFlight > concurrency) {
        exceededLimit = true;
      }
      inFlight++;
      await delay(Math.floor(Math.random() * 50));
      inFlight--;
      return n;
    };
    for await (const value of execute(values, f, concurrency, false)) {
      actualValues.push(value);
    }
    expect(actualValues.sort((a, b) => a - b)).toEqual(values);
    expect(exceededLimit).toBe(false);
  });

  it('achieves optimum concurrency', async () => {
    const actualValues = new Array<number>();
    const concurrency = 17;
    let inFlight = 0;
    const deviationAllowed = 3;
    const values = [...Array(100).keys()];
    let concurrencyReached = false;
    let concurrencyReduced = false;
    const f = async (n: number) => {
      inFlight++;
      if (inFlight === concurrency) {
        concurrencyReached = true;
      }
      if (concurrencyReached && inFlight < (concurrency - deviationAllowed)) {
        concurrencyReduced = true;
      }
      await delay(Math.floor(Math.random() * 50));
      inFlight--;
      return n;
    };
    for await (const value of execute(values, f, concurrency, false)) {
      actualValues.push(value);
    }
    expect(actualValues.sort((a, b) => a - b)).toEqual(values);
    expect(concurrencyReached).toBe(true);
    expect(concurrencyReduced).toBe(false);
  });

  it('exercises back pressure', async () => {
    const concurrency = 1;
    const actualValues = new Array<number>();
    async function* numberGenerator(): AsyncIterable<number> {
      for (const value of tenNumbers) {
        await delay(50);
        yield value;
      }
    }
    let inFlight = 0;
    let tooMuchPressure = false;
    const f = async (n: number) => {
      if (inFlight > 1) {
        tooMuchPressure = true;
      }
      inFlight++;
      await delay(50);
      inFlight--;
      return n;
    };
    for await (const value of execute(numberGenerator(), f, concurrency, true)) {
      actualValues.push(value);
    }
    await delay(1);
    expect(actualValues.sort((a, b) => a - b)).toEqual(tenNumbers);
    expect(tooMuchPressure).toBe(false);
  });

  it('back pressure does not swallow errors', async () => {
    const concurrency = 1;
    const error = new Error('boom 3');
    const actualValues = new Array<number>();
    async function* numberGenerator(): AsyncIterable<number> {
      for (const value of tenNumbers) {
        await delay(50);
        if (value === 5) {
          throw error;
        }
        yield value;
      }
    }
    const f = async (n: number) => {
      await delay(50);
      return n;
    };
    try {
      for await (const value of execute(numberGenerator(), f, concurrency, true)) {
        actualValues.push(value);
      }
      fail('Expected asynchronous generator iteration to fail');
    } catch (err) {
      if (err !== error) {
        throw err;
      }
    }
    expect(actualValues.sort((a, b) => a - b)).toEqual([0, 1, 2, 3, 4]);
  });

  it('handles undefined arguments', async () => {
    expect(() => execute(undefined as any as Array<number>, ((n) => delay(100).then(() => n)), 3, false)).toThrow('Unrecognized source of data');
  });

  it('handles invalid arguments', async () => {
    expect(() => execute(42 as any as Array<number>, ((n) => delay(100).then(() => n)), 3, false)).toThrow('Unrecognized source of data');
  });

  it('supports 1-value iterator', async () => {
    const ait = {
      [Symbol.asyncIterator]: () => {
        return {
          next: () => Promise.resolve({done: true, value: 42}),
          return: () => Promise.resolve({done: true, value: 42}),
          throw: (e?: Error) => Promise.reject(e)
        };
      }
    };
    const f = async (n: number) => {
      await delay(50);
      return n;
    };
    const it = execute(ait, f);
    await delay(1);
    for await (const value of it) {
      expect(value).toEqual(42);
    }
  });

  it('exercises break with back pressure', async () => {
    const concurrency = 2;
    const actualValues = new Array<number>();
    let lastStatementReached = false;
    async function* numberGenerator(): AsyncIterable<number> {
      try {
        for (const value of tenNumbers) {
          await delay(50);
          yield value;
        }
      } finally {
        lastStatementReached = true;
      }
    }
    let inFlight = 0;
    let tooMuchPressure = false;
    const f = async (n: number) => {
      if (inFlight > 1) {
        tooMuchPressure = true;
      }
      inFlight++;
      await delay(50);
      inFlight--;
      return n;
    };
    for await (const value of execute(numberGenerator(), f, concurrency, true)) {
      actualValues.push(value);
      if (value === 5) {
        break;
      }
    }
    await delay(1);
    expect(lastStatementReached).toBe(true);
    expect(tooMuchPressure).toBe(false);
  });

});
